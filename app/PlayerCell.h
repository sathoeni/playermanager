//
//  PlayerCell.h
//  StoryboardExampleApp
//
//  Created by Sascha Thöni on 20.06.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *gameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *ratingImageView;

@end
