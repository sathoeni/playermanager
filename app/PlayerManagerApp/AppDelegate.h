//
//  AppDelegate.h
//  StoryboardExampleApp
//
//  Created by Sascha Thöni on 18.06.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
