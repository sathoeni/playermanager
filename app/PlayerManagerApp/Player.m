//
//  Player.m
//  StoryboardExampleApp
//
//  Created by Sascha Thöni on 18.06.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "Player.h"

@implementation Player

@synthesize name;
@synthesize game;
@synthesize rating;

@end
