#import "../tuneup/tuneup.js"

test("Test 1", function(target, app) {

    var window = app.mainWindow();
    var target = UIATarget.localTarget();
	var selectedTabName;
    //app.logElementTree();


	//-- select the elements
	UIALogger.logMessage( "Select the first tab" );
	var tabBar = app.tabBar();
	selectedTabName = tabBar.selectedButton().name();
	assertEquals("Players", selectedTabName);

if (selectedTabName != "Gestures") {
    tabBar.buttons()["Gestures"].tap();
}
target.delay(1);

selectedTabName = tabBar.selectedButton().name();
assertEquals("Gestures", selectedTabName);	
	 

//window.navigationBar().name()["Welcome"].withValueForKey(1, "isVisible");


if (selectedTabName != "Players") {
    tabBar.buttons()["Players"].tap();
}
	 
//UIALogger.logStart("Logging element tree …");
//UIATarget.localTarget().logElementTree();
		 

//get the current window	 
var mainWindow = app.mainWindow(); 	 
		 
//print all the players available
UIALogger.logMessage(mainWindow.tableViews()[0].name());
var players = mainWindow.tableViews()[0].cells();
for (var i = 0; i < players.length; i++) {
    UIALogger.logMessage(players[i].name());
    //Do something
}
	 	 
//-- add a Player
UIATarget.localTarget().pushTimeout(2);
app.navigationBar().buttons()["Add"].withValueForKey(1, "isVisible");
app.navigationBar().buttons()["Add"].tap();
UIATarget.localTarget().popTimeout();
	 
 	 
//var mainWindow = app.mainWindow(); 
//log current elementTree
//mainWindow.logElementTree();	 
		 
//-- set the player name
UIATarget.localTarget().pushTimeout(2);
mainWindow.tableViews()[0].cells()[0].elements()[0].withValueForKey(1, "isVisible");
var nameTextField = mainWindow.tableViews()[0].cells()[0].elements()[0];
nameTextField.setValue("Moby");
	 
//-- open "choose game" screen by tapping the game button
var gameButton = mainWindow.tableViews()[0].cells()[1];//elements()[0];
gameButton.tap();
UIATarget.localTarget().popTimeout();
		 

//get the current window	 
//var mainWindow = app.mainWindow(); 
//log current elementTree
//mainWindow.logElementTree();
		 
		 
//choose a game
UIATarget.localTarget().pushTimeout(2);
mainWindow.tableViews()[0].cells()[gameNumber].withValueForKey(1, "isVisible");
var gameNumber = 4;
var aGame = mainWindow.tableViews()[0].cells()[gameNumber];
aGame.tap();
UIATarget.localTarget().popTimeout();


//tab the Done button
UIATarget.localTarget().pushTimeout(2);
app.navigationBar().buttons()["Done"].withValueForKey(1, "isVisible");	 
UIATarget.localTarget().popTimeout();
app.navigationBar().buttons()["Done"].tap();
		 
});